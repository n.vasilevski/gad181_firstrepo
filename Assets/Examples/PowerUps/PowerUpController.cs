﻿using UnityEngine;

namespace SAE.Example
{
    public class PowerUpController : MonoBehaviour
    {
        [SerializeField] protected Weapon weapon;

        [SerializeField]
        protected Projectile defaultProjectile,
                             missilesProjPrefab,
                             bombProjPrefab,
                             plasmaProjPrefab;

        [SerializeField] protected float powerUpTime = 30;
        protected float powerUpTimeCounter;

        public void Update()
        {
            if (powerUpTimeCounter >= 0)
            {
                powerUpTimeCounter -= Time.deltaTime;

                if (powerUpTimeCounter <= 0)
                {
                    RemoveAllPowerUps();
                }
            }
        }

        public void ReceivePowerUp(PowerUpBehaviour.PowerUpType powerUpType)
        {
            //remove any current powerup
            RemoveAllPowerUps();

            //set weapon based on type
            switch (powerUpType)
            {
                case PowerUpBehaviour.PowerUpType.Missiles:
                    weapon.SetProjectile(missilesProjPrefab);
                    break;

                case PowerUpBehaviour.PowerUpType.Bomb:
                    weapon.SetProjectile(bombProjPrefab);
                    break;

                case PowerUpBehaviour.PowerUpType.Plasma:
                    weapon.SetProjectile(plasmaProjPrefab);
                    break;

                default:
                break;
            }

            //reset timer
            powerUpTimeCounter = powerUpTime;

            //EFFECTS!
        }

        public void RemoveAllPowerUps()
        {
            //put all back to defaults
            weapon.SetProjectile(defaultProjectile);
        }
    }
}
