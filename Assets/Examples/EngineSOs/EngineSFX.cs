﻿using UnityEngine;

namespace SAE.Example
{
    public class EngineSFX : MonoBehaviour
    {
        [SerializeField] protected PlaneMovement planeMovement;
        [SerializeField] protected AudioSource engineAudioSource;
        [SerializeField] protected ValueMappedAudioDrone engineDrone = new ValueMappedAudioDrone();

        public virtual void Start()
        {
            if (engineAudioSource != null)
            {
                engineDrone.Init(planeMovement.CurrentThurstPercentage, engineAudioSource);
            }
        }

        private void Update()
        {
            if (engineAudioSource != null)
            {
                engineDrone.Tick(planeMovement.CurrentThurstPercentage, Time.deltaTime, engineAudioSource);
            }
        }

        public void CopyFrom(ValueMappedAudioDrone other)
        {
            engineDrone.CopyFrom(other);
        }

        public void SetAudioClip(AudioClip clip)
        {
            engineAudioSource.clip = clip;
            engineAudioSource.Play();
        }
    }
}
