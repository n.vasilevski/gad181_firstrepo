﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// This is much simplier than the plane AI. It chooses a direction at spawn, moves
    /// in that direction for it's lifetime and shoots at the player.
    /// </summary>
    public class EnemyShipController : MonoBehaviour
    {
        [SerializeField] protected TurretRotationController[] turrets;
        [SerializeField] protected float maxFireDistance = 10;
        [SerializeField] protected float shootRate = 5;
        [SerializeField] protected Vector2 moveSpeedRange = new Vector2(-2, 2);
        [SerializeField] protected GameObjectCollectionSO playerCollection;
        protected Weapon[] turretWeapons;
        protected float selectedSpeed;
        protected float shootRateCounter;

        public void Start()
        {
            CollectionWeapons();

            selectedSpeed = Random.Range(moveSpeedRange.x, moveSpeedRange.y);
        }

        private void CollectionWeapons()
        {
            turretWeapons = new Weapon[turrets.Length];
            for (int i = 0; i < turrets.Length; i++)
            {
                turretWeapons[i] = turrets[i].GetComponent<Weapon>();
            }
        }

        public virtual void Update()
        {
            if (playerCollection.First == null)
            {
                //TODO do something that makes more sense than engines off
                return;
            }

            var playerPos = playerCollection.First.transform.position;

            bool wantsToFire = false;
            shootRateCounter += Time.deltaTime;
            if (shootRateCounter > shootRate && Vector3.Distance(transform.position, playerPos) < maxFireDistance)
            {
                wantsToFire = true;
            }

            UpdateWeaponTracking(playerPos, wantsToFire);

            transform.Translate(Vector3.left * selectedSpeed * Time.deltaTime);
        }

        private void UpdateWeaponTracking(Vector3 playerPos, bool wantsToFire)
        {
            for (int i = 0; i < turrets.Length; i++)
            {
                var item = turrets[i];
                item.TargetPosition = playerPos;
                if (wantsToFire && turretWeapons[i] != null)
                {
                    turretWeapons[i].Fire();
                }
            }
        }

        public void OnDrawGizmosSelected()
        {
            var curCol = Gizmos.color;
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, maxFireDistance);
            Gizmos.color = curCol;
        }
    }
}
