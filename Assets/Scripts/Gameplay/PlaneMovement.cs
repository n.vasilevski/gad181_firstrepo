﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Mimics that found in LUFTRAUSERS, turning when boosting syncs forwards, turning when not boosting
    /// rotates only. Intended to function with gravity and variable drag.
    /// </summary>
    public class PlaneMovement : MonoBehaviour
    {
        [SerializeField] protected Rigidbody2D rb;

        [Tooltip("Engine force output, countered by drag.")]
        [SerializeField] protected float engineForce;

        [Tooltip("Max rotational speed of the plane")]
        [SerializeField] protected float rotateSpeed = 1;

        [Tooltip("Scaling factor on rotation of plane when the player is boosting while rotating.")]
        [SerializeField] protected float boostRotRatio = 0.5f;

        public float EngineForce
        {
            get { return engineForce; }
            set { engineForce = value; }
        }

        public float RotateSpeed
        {
            get { return rotateSpeed; }
            set { rotateSpeed = value; }
        }

        public float BoostRotRatio
        {
            get { return boostRotRatio; }
            set { boostRotRatio = value; }
        }

        public float CurrentThurstPercentage { get; protected set; }
        public float CurrentRotatePercentage { get; protected set; }

        /// <summary>
        /// Called to update plane engine and 'control surfaces'.
        /// </summary>
        /// <param name="thrustPer">0-1 expected</param>
        /// <param name="rotPer">-1 to 1 expected</param>
        public virtual void SetEngineState(float thrustPer, float rotPer)
        {
            CurrentThurstPercentage = Mathf.Clamp(thrustPer, 0, 1.0f);
            CurrentRotatePercentage = Mathf.Clamp(rotPer, -1.0f, 1.0f);
        }

        public virtual void FixedUpdate()
        {
            ApplyMovement(Time.fixedDeltaTime);
        }

        private void ApplyMovement(float step)
        {
            var rotAmt = CurrentRotatePercentage * step * rotateSpeed;

            if (Mathf.Abs(CurrentRotatePercentage) > Mathf.Epsilon)
            {
                rb.MoveRotation(rb.rotation + rotAmt);
            }

            if (Mathf.Abs(CurrentThurstPercentage) > Mathf.Epsilon)
            {
                rotAmt *= boostRotRatio;

                rb.velocity = Quaternion.Euler(0, 0, rotAmt) * rb.velocity;

                rb.AddRelativeForce(Vector2.up * engineForce * CurrentThurstPercentage);
            }
        }
    }
}
