﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Rotate a transform towards a given location, includes simple max per frame rate limit.
    /// </summary>
    public class TurretRotationController : MonoBehaviour
    {
        [SerializeField] protected Transform turretToRotate;
        [SerializeField] protected float rotateRate = 360;
        public Vector3 TargetPosition { get; set; }

        public void Start()
        {
            TargetPosition = transform.position + Vector3.up;
        }

        public virtual void Update()
        {
            //this can tell you angle
            //var ang = Vector3.SignedAngle(turretToRotate.up, targetDir, Vector3.forward);

            var targetDir = (TargetPosition - turretToRotate.position).normalized;
            var rot = Quaternion.LookRotation(Vector3.forward, targetDir);
            var newRot = Quaternion.RotateTowards(turretToRotate.rotation, rot, rotateRate * Time.deltaTime);
            turretToRotate.rotation = newRot;

            //OR this method
            var angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90;
            //if using to rotate something by
            var q = Quaternion.AngleAxis(angle, Vector3.forward);
            //or
            var fromEuler = Quaternion.Euler(0, 0, angle);

            //var curEuler = turretToRotate.eulerAngles;
            //curEuler.z = angle;
            //turretToRotate.eulerAngles = curEuler;
        }
    }
}
