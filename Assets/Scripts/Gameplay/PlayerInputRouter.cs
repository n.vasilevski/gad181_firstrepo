﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Simple gather of user input, converted to plane movement desired. This separation is intended to
    /// keep the componenets responsible for moving objects clean and free of player centric and/or
    /// input code so they can be resuled by NPCs/Enemies etc.
    /// </summary>
    public class PlayerInputRouter : MonoBehaviour
    {
        [SerializeField] protected PlaneMovement pm;
        [SerializeField] protected Weapon weapon;
        [SerializeField] protected string thrustAxisName = "Vertical";
        [SerializeField] protected string rotateAxisName = "Horizontal";
        [SerializeField] protected KeyCode keyCode = KeyCode.X;

        private void Update()
        {
            pm.SetEngineState(Input.GetAxisRaw(thrustAxisName), -Input.GetAxisRaw(rotateAxisName));
            if (Input.GetKey(keyCode))
            {
                weapon.Fire();
            }
        }
    }
}
