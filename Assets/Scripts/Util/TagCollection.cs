﻿using System.Collections.Generic;
using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Simple util class for comparing a tag against a collection or cloud of tags.
    ///
    /// At present very simple but allows for easy expansion should there be a need or performance imperative.
    /// </summary>
    [System.Serializable]
    public class TagCollection
    {
        [SerializeField] protected List<string> tags = new List<string>();
        [SerializeField] protected bool emptyMeansAll = true;

        public bool Contains(string tag)
        {
            return (tags.Count == 0 && emptyMeansAll) || tags.IndexOf(tag) != -1;
        }

        public void Add(string tag)
        {
            tags.Add(tag);
        }

        public void Remove(string tag)
        {
            tags.Remove(tag);
        }
    }
}
